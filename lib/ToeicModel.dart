class ToeicModel {
  late int no;
  late String image;
  late String audio;

  ToeicModel(this.no, this.image, this.audio);

  ToeicModel.fromJson(jsonObject) {
    no = jsonObject['no'];
    image = jsonObject['image'];
    audio = jsonObject['audio'];
  }

  @override
  String toString() {
    return '$no $image $audio';
  }
}