import 'package:flutter/material.dart';
import 'package:toeic/ToeicModel.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'detail.dart';


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'TOEIC part 1',
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyState();
  }
}

class MyState extends State<MyStatefulWidget>{
  int counter = 0;
  String urlString = 'https://thachln.github.io/toeic-data/ets/2016/1/p1/';
  late List<ToeicModel> questions = [];

  @override
  void initState() {
    super.initState();
    readJSON();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Đề thi tiếng Anh chuẩn TOEIC - phần 1')),
      body: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: questions.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              margin: const EdgeInsets.all(2.0),
              height: 250.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(urlString + questions[index].image),
                  fit: BoxFit.fitWidth,
                  )
              ),
              alignment: Alignment.center,
              child: InkWell(
                onTap: () {
                  print("${questions[index].image} is clicked.");
                  Navigator.push(context,
                    MaterialPageRoute(
                      builder: (context) => DetailScreen(),//questions[index]),
                    // Pass the arguments as part of the RouteSettings. The
                    // DetailScreen reads the arguments from these settings.
                      settings: RouteSettings(
                        arguments: questions[index],
                      )
                    )
                  );
                }
              )

              // using ListTile way instead of Container
          // title: Text(questions[index].no.toString()),
          // leading: Image.network(urlString + questions[index].image),
          );
        }
      ),

    );
  }

  readJSON() async {
    String urlString = 'https://thachln.github.io/toeic-data/ets/2016/1/p1/';
    var response = await http.read(Uri.parse(urlString + 'data.json'));
    var jsonObject = json.decode(response);

    for (int i = 0; i < jsonObject.length; i++) {
      questions.add(ToeicModel.fromJson(jsonObject[i]));
    }
  }

}

