import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toeic/ToeicModel.dart';

class DetailScreen extends StatefulWidget {

    // pass data between pages using constructors
    // then use widget.question.img like this to call reference
  // var question;
  // DetailScreen(this.question);

  @override
  State<StatefulWidget> createState() {
    return MyDetailState();
  }


}

class MyDetailState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    var question = ModalRoute.of(context)!.settings.arguments as ToeicModel;

    return Scaffold(
      appBar: AppBar(
        title: Text("Question number #${question.no}"),
      ),
      // body: Text(question!.no),
      // Padding(
      //   padding: const EdgeInsets.all(16.0),
      //   child: Text(todo.description),
      // ),
    );
  }

}